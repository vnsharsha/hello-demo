.PHONY: lint test start

# The build rule is designed to be run locally as well as from the ci tool.
# These variables should be supplied by the CI tool,
# but are set to sane defaults when running locally.
IMAGE := $(or $(IMAGE), hello-demo)
BUILD_TAG := $(or $(BUILD_TAG), build)
RELEASE_TAG := $(or $(RELEASE_TAG), edge)

# Builds both build and release-tagged images
# The image created during the build contains the environment and scripts to do pre and post-deployment testing.
# We first build the image, then execute the commit_test.sh script in a container based on the image,
# being sure to mount the local reports folder, so that we can capture the output of the tests,
# and expose it to our CI server.
build: set-executable ensure-reports
	# build the base stage's image, which can run test scripts
	docker build \
		--target build \
		--cache-from $(IMAGE):$(BUILD_TAG) \
		--tag $(IMAGE):$(BUILD_TAG) \
		.

	# run the commit test script against the image we just built
	# the "//" on the target of the bind is so that windows machines don't choke (cygwin).
	docker run \
		--rm \
		--mount type=bind,source=$(shell pwd)/reports,target=//app/reports \
		$(IMAGE):$(BUILD_TAG) \
		abin/commit-test.sh

	# now build the release stage's image
	docker build \
		--cache-from $(IMAGE):$(BUILD_TAG) \
		--cache-from $(IMAGE):$(RELEASE_TAG) \
		--tag $(IMAGE):$(RELEASE_TAG) \
		.

# Run the program from the built container
run:
	docker run --rm \
		-p 3000:3000 \
		-e NODE_ENV=development \
		$(IMAGE):$(RELEASE_TAG) \
		| npx bunyan --output short --time local

pack-chart: set-executable
	./abin/pack-chart.sh

# Deploy the application via helm chart
deploy: set-executable
	./abin/deploy.sh

lint:
	@npm run --silent lint

test: ensure-reports set-executable
	@./abin/commit-test.sh

ensure-reports:
	mkdir -p reports

start:
	@npm run --silent start

# =============
# Simple test rules used when running locally (after `make start`)
# =============

curl200:
	@curl -w "\n%{http_code}\n" localhost:3000/songs/7

curl404:
	@curl -w "\n%{http_code}\n" localhost:3000/songs/0

curlhealth:
	@curl -w "\n%{http_code}\n" localhost:3000/health

set-executable:
	chmod +x abin/*.sh
